#!/bin/bash

gradle clean fatJar
docker build -t jar .
#docker build -t jar .  --no-cache
docker run -d --name jar -v /home/$USERNAME/.ssh/:/root/.ssh/ -v /home/$USERNAME/workdir/:/application/workdir jar
