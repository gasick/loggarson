package main.commands

import main.menu.Commands
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update

internal fun Commands.startCommand(update: Update?) {
    execute(
        SendMessage()
            .setChatId(update!!.message.chatId)
            .setText(
                "Прифки!\n" +
                        "Пробуй: /help"
            )
    )
}