package main.commands

import main.configs.Logger
import main.chatConfigCache
import main.menu.Commands
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update

internal fun Commands.infoCommands(update: Update) {
    val chatConfig = chatConfigCache.usersList.filter{it.chatId == update.message.chatId}[0]
    var message = ""
    for (i in 0..chatConfig.serverList.size-1) {
        message += "$i - " + chatConfig.serverList[i] +"\n"
    }
    Logger.d(message)
    execute(
        SendMessage()
            .setChatId(update.message.chatId)
            .setText(
                "У вас есть возможность работать с хостами: \n\n" +
                        "$message\n" +
                        "Если не указан хост, по умолчанию срабатывает на хосте под номером 0.\n" +
                        "Но это не точно"
            )
    )
}
