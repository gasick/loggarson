package main.menu

import main.configs.Constants
import org.telegram.telegrambots.bots.DefaultAbsSender
import org.telegram.telegrambots.bots.DefaultBotOptions

class Commands (options: DefaultBotOptions?) : DefaultAbsSender(options) {

    override fun getBotToken() = Constants.BotToken

}
