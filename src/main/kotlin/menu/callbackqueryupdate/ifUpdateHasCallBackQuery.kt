package main.commands.bot

import main.menu.Commands
import main.configs.Logger
import org.telegram.telegrambots.meta.api.objects.Update


internal fun Commands.ifUpdateHasCallBackQuery(update: Update) {
    Logger.d("Entering ifUpdateHasCallBackQuery")
    Logger.d("Содержание ответа обратного вызова: " + update.callbackQuery.data)

    when  {
        update.callbackQuery.data.contains("getLogsFromService") ->{ getLogsFromService(update)}
        update.callbackQuery.data.contains("chosenHost") ->  chooseServiceToGetLogs(update, true)
        update.callbackQuery.data.contains("backToHostList") -> {updateHostToGetServicesList(update)}
    }

}

