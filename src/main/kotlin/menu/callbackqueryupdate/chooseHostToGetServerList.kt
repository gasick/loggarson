package main.commands.bot

import main.configs.Logger
import main.chatConfigCache
import main.menu.Commands
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton


internal fun Commands.chooseHostToGetServicesList(update: Update) {
    Logger.d("Зашли в  chooseHostToGetServicesList")
    var list = mutableListOf<MutableList<InlineKeyboardButton>>()
    Logger.d(chatConfigCache.usersList.filter { it.chatId == update.message.chatId}[0].serverList.toString())
    chatConfigCache.usersList.filter{ it.chatId == update.message.chatId}[0].serverList.forEach {
        list.add(mutableListOf(InlineKeyboardButton().apply {
            text = it
            Logger.d(text)
            callbackData = "choosenHost:$it"
        }))
    }
    execute(
        SendMessage()
            .setText("Выберите хост")
            .setChatId(update.message.chatId)
            .setReplyMarkup(InlineKeyboardMarkup().apply {
                keyboard = list
            })
    )
}

internal fun Commands.updateHostToGetServicesList(update: Update){
    Logger.d("Зашли в  updateHostToGetServicesList")
    var list = mutableListOf<MutableList<InlineKeyboardButton>>()
    Logger.d(chatConfigCache.usersList.filter {it.chatId == update.callbackQuery.message.chatId}[0].serverList.toString())
    chatConfigCache.usersList.filter{ it.chatId == update.callbackQuery.message.chatId}[0].serverList.forEach {
        list.add(mutableListOf(InlineKeyboardButton().apply {
            text = it
            callbackData = "choosenHost:$it"
        }))
    }
    var emrm = EditMessageReplyMarkup().apply {
        chatId = update.callbackQuery.message.chatId.toString()
        messageId = update.callbackQuery.message.messageId
        replyMarkup = InlineKeyboardMarkup().apply {
            keyboard = list
        }
    }
    execute(emrm)
    //execute(
    //    SendMessage()
    //        .setText("Выберите хост")
    //        .setChatId(update.message.chatId)
    //        .setReplyMarkup(InlineKeyboardMarkup().apply {
    //            keyboard = list
    //        })
    //)
}
