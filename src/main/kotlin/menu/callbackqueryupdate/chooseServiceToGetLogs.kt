package main.commands.bot

import main.configs.Logger
import main.workers.ssh.returnServices
import main.chatConfigCache
import main.menu.Commands
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton


internal fun Commands.chooseServiceToGetLogs(update: Update, withBack: Boolean = false) {
    when (withBack) {
//Выбираем сервисы
// Если это новый запрос, то мы просто создаем новое сообщение с возможностью выбора
// сервиса которое можно получить
        false -> {
            Logger.d("Entering chooseServiceToGetLogs without back")
            var list = mutableListOf<MutableList<InlineKeyboardButton>>()

            returnServices(update.message.chatId).forEach {
                list.add(mutableListOf(InlineKeyboardButton().apply {
                    text = it
                    callbackData = "getLogsFromService:0:$it"
                }))
            }

            execute(
                SendMessage()
                    .setText("Выберите сервис")
                    .setChatId(update.message.chatId)
                    .setReplyMarkup(InlineKeyboardMarkup().apply {
                        keyboard = list
                    })
            )
        }
//Обновляем сообщение в котором был указан список хостов
        true -> {
            Logger.d("Entering chooseServiceToGetLogs with back")
            Logger.d(update.callbackQuery.data)
            Logger.d("Апдатим меню с хостами на меню с ")
            Logger.d(update.callbackQuery.message.chatId.toString())
            Logger.d(chatConfigCache.usersList.filter { it.chatId == update.callbackQuery.message.chatId }.toString())
            Logger.d(
                chatConfigCache.usersList.filter { it.chatId == update.callbackQuery.message.chatId }[0].serverList.indexOf(
                    update.callbackQuery.data.removePrefix("choosenHost:")
                ).toString()
            )
            var list = mutableListOf<MutableList<InlineKeyboardButton>>()
            Logger.d("еще разок адаптим")
            Logger.d(update.callbackQuery.message.chatId)
            returnServices(
                update.callbackQuery.message.chatId,
                chatConfigCache.usersList.filter { it.chatId == update.callbackQuery.message.chatId }[0].serverList.indexOf(
                    update.callbackQuery.data.removePrefix("choosenHost:")
                )
            ).forEach {
                list.add(mutableListOf(InlineKeyboardButton().apply {
                    text = it.toString()
                    callbackData = "getLogsFromService:" +
                            "${
                                chatConfigCache.usersList.filter{ 
                                    it.chatId == update.callbackQuery.message.chatId
                                }[0].serverList.indexOf(update.callbackQuery.data.removePrefix("choosenHost:"))
                            }" +
                            ":$it"
                }))
            }
            list.add(
                mutableListOf(InlineKeyboardButton().apply {
                    text = "Назад"
                    callbackData = "backToHostList"
                }
                )
            )
            try {
                Logger.d("пришли траить")
                var emrm = EditMessageReplyMarkup().apply {
                    chatId = update.callbackQuery.message.chatId.toString()
                    messageId = update.callbackQuery.message.messageId
                    replyMarkup = InlineKeyboardMarkup().apply {
                        keyboard = list
                    }
                }
                execute(emrm)

            } catch (e: Exception) {
                e.stackTrace
            }
        }
    }
}
