package main.configs

import java.io.File
import java.lang.System.getenv

object Constants {
    private var workDirectory: String = getenv("WORKDIRECTORY") ?: "./workdir/"
    private var chatsConfigurationFile: String = getenv("CHATCONFIGURATIONPATH") ?: "chatsConfiguration/"
    private var scriptsConfigurationFile: String = getenv("SCRIPTCONFIGURATIONPATH") ?: "scriptConfiguration/"
    private var tempDirectoryForLogFile: String = getenv("TEMPDIRECTORYFORLOGFILE") ?: "templogfiles/"
    private var scriptsConfigurationFilePath = workDirectory + scriptsConfigurationFile
    var debug = (getenv("DEBUG") ?: "true").toBoolean()
    var adminId = listOf("")
    var BotUserName: String = getenv("BOTUSERNAME") ?: ""
    var BotToken: String = getenv("BOTTOKEN") ?: ""
    var chatsConfigurationFilePath = workDirectory + chatsConfigurationFile
    var tempDirectoryForLogFilePath = workDirectory + tempDirectoryForLogFile


    fun checkDirectories() {
        File(workDirectory).mkdir()
        File(chatsConfigurationFilePath).mkdir()
        File(tempDirectoryForLogFilePath).mkdir()
        File(scriptsConfigurationFilePath).mkdir()
    }
}
