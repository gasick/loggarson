package main.configs

import main.configs.Constants.debug
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Logger {
    private var dtf  = DateTimeFormatter.ofPattern("HH:mm:ss dd-MM-yyyy")
    fun e(message: Any) {
        println( LocalDateTime.now().format(dtf)  + " - error: " + message)
    }
    fun w(message: Any) {
        println( LocalDateTime.now().format(dtf) + " - warn: " + message)
    }
    fun d(message: Any) {
        if (debug) println( LocalDateTime.now().format(dtf) + " - debug: " + message)
    }
    fun i(message: Any) {
        println( LocalDateTime.now().format(dtf) + " - info: " + message)
    }
    fun t(message: Any) {
        println( LocalDateTime.now().format(dtf) + " - trace: " + message)
    }
    fun f(message: Any) {
        println( LocalDateTime.now().format(dtf) + " - fatal: " + message)
    }
}