package main

import main.commands.*
import main.commands.bot.ifUpdateHasCallBackQuery
import main.configs.Constants
import main.configs.Logger
import main.menu.ScriptsCache
import main.menu.scripts.addScriptToList
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.bots.DefaultBotOptions
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.ApiContext
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.objects.Update


fun main() {
    Constants.checkDirectories()
    chatConfigCache.loadUserCache()
    ApiContextInitializer.init()
    TelegramBotsApi().registerBot(Bot())



}

class Bot : TelegramLongPollingBot() {
    override fun getBotUsername() = Constants.BotUserName
    override fun getBotToken() = Constants.BotToken
    private var Commands = main.menu.Commands(ApiContext.getInstance(DefaultBotOptions::class.java))

    @ExperimentalStdlibApi
    override fun onUpdateReceived(update: Update?) {
        Logger.d(update!!)
        Logger.d("Зашли в onUPdateReceived")

        when {
            // реакция на нажатие ссылки в телеграме
            update.hasCallbackQuery() -> {
                Logger.d("Зашли в  hasCallbackQuery")
                Commands . ifUpdateHasCallBackQuery (update)
            }
            //реакция на входящий текстовый файл
            update.message.hasDocument()  -> {
                Logger.d("Зашли в  hasDocument")
                ScriptsCache.addScriptToList(update)
            }
            //реакция на входящее сообщение
            update.hasMessage()   -> {
                Logger.d("Зашли в hasMessage")
                when {
                    update.message.text.contains("/start") -> Commands.startCommand(update)
                    update.message.text.contains("/help") -> Commands.helpCommand(update)
                    update.message.text.contains("/bot") -> Commands.botCommands(update)
                    update.message.text.contains("/cmd") -> Commands.cmdCommands(update)
                    update.message.text.contains("/info") -> Commands.infoCommands(update)
                    update.message.text.contains("/set") -> Commands.setCommands(update)
                    //update.message.text.contains("/script") -> Commands.
                }
            }


        }
    }

}



