package main.workers.ssh

import main.configs.Constants
import main.configs.Logger
import main.chatConfigCache
import main.workers.ServiceLogs
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*

fun returnLogs(serviceName: String, chatId: Long, serverNumber: Int = 0 ): ServiceLogs {
    Logger.d("Зашли в возврат логов")
    val data = SimpleDateFormat("dd-MM-yy_hh-mm-ss").format(Date())
    val fileObject = ServiceLogs(logFile = File(Constants.tempDirectoryForLogFilePath, "$serviceName#$data")).apply {
//        println("зашли в логфиле")
        trimmedLogs.add(serviceName)
    }

    val rt = Runtime.getRuntime()
    val cConfig = chatConfigCache.usersList.filter { it.chatId == chatId }[0]
    val command = "ssh -o ForwardX11=no ${cConfig.serverList[serverNumber]}  docker logs $serviceName --tail 100   "
    val proc = rt.exec(command)
    val stdInput = BufferedReader(InputStreamReader(proc.inputStream))
    val stdError = BufferedReader(InputStreamReader(proc.errorStream))
// Read the output from the command
    Logger.e(stdError)
    var s: String?
    val tempLogs  = mutableListOf<String>()
    while (stdInput.readLine().also { s = it } != null) {
        tempLogs.add(s!!)
    }
// Read any errors from the command attempt
    while (stdError.readLine().also { s = it } != null) {
        tempLogs.add(s!!)
    }

    fileObject.trimmedLogs = tempLogs.takeLast(100).toMutableList()
    fileObject.trimmedLogs.forEach{fileObject.logFile.appendText(it+"\n")}


    return fileObject
}
