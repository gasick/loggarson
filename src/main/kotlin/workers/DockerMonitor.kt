package main.workers

object DockerMonitor {
    enum class DockerState{ RUNNING, STOPPED }
    data class hostDockerState (
        var hostname: String,
        var servicesState: MutableList<Pair<String, DockerState>>
    )
}