package main.workers

import java.io.File

data class ServiceLogs(
    var trimmedLogs: MutableList<String> = mutableListOf(),
    var logFile: File
)
